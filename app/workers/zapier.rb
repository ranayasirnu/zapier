require 'resque-history'

class Zapier
  extend Resque::Plugins::History
  @queue = :zapier
  @base_url = "https://zapier.com/hooks/catch/bim8gd/"

  def self.perform(zap_id, params)
    zap_url = "#{@base_url}#{zap_id}/"
    RestClient.post(zap_url, params)
  end

  def self.zap(zap_name, params)
    zap_id = ZAP_IDS[zap_name.to_s]
    if zap_id.nil?
      Rails.logger.error "No zap id found for zap with name '#{zap_name}'. Check zap_ids.yml"
      return
    end
    Rails.logger.info "zapping #{zap_id} with params: #{params.to_json}"
    if Rails.env.production?
      Resque.enqueue(Zapier, zap_id, params)
    end
  end
end