class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable
  require "restclient"

  require 'resque-history'
  before_update :see_if_favorite_beer_changed
  class Zapier
    extend Resque::Plugins::History
    @queue = :zapier
    @base_url = "https://zapier.com/hooks/catch/"

    def self.perform(zap_id, params)
      zap_url = "#{@base_url}#{zap_id}/"
      puts "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa#{zap_url.inspect}"
      RestClient.post(zap_url, params)
    end

    def self.zap(zap_name, params)
      zap_id = ZAP_IDS[zap_name.to_s]
      if zap_id.nil?
        Rails.logger.error "No zap id found for zap with name '#{zap_name}'. Check zap_ids.yml"
        return
      end
      Rails.logger.info "zapping #{zap_id} with params: #{params.to_json}"
      if Rails.env.development?
        puts "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
        Resque.enqueue(Zapier, zap_id, params)
      end
    end
  end


  def see_if_favorite_beer_changed
    puts favorite_beer_changed?
    begin
      if email_changed?
        params = { email: email, favorite_beer: favorite_beer }
        Zapier.perform('bikryb', params)
      end
    rescue
      logger.error "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
    end
  end

end
